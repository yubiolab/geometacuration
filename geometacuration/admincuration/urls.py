from django.shortcuts import render
from django.conf.urls import url,include
from . import views
from django.contrib.auth.views import login,logout


urlpatterns = [
    url(r'^$', views.session_login),
    url(r'^login/$', login, {'template_name':'admincuration/login.html'}),
    url(r'^ad/$',views.ad,name='ad'),
    url(r'ad/assign/$', views.assign,name = 'assign'),
    url(r'ad/create/$', views.create,name = 'create'),
    ]
