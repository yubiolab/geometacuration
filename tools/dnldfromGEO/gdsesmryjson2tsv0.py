#!/usr/bin/env python

import sys
import json
json_obj = json.load(sys.stdin)

import pprint
for k, v in json_obj[u'result'].iteritems():
	if k != u'uids':
		print('\t'.join((
			v[u'accession']
			, v[u'title']
			, str(v[u'n_samples'])
			, v['gdstype']
			, ';'.join([u'GPL' + x for x in v[u'gpl'].split(';')])
			, v[u'taxon']
			, ','.join([str(x) for x in v[u'pubmedids']])
			, v[u'summary']
			)).encode('utf-8'))
