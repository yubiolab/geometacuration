#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:


keyword='(rosiglitazone[Description] OR rosiglitazone[Title]) AND ("Homo sapiens"[porgn:__txid9606]) AND gse[Entry Type] AND (("expression profiling by array"[DataSet Type] OR "non coding rna profiling by array"[DataSet Type]) OR ("expression profiling by high throughput sequencing"[DataSet Type] OR "non coding rna profiling by high throughput sequencing"[DataSet Type])) NOT SuperSeries[All Fields]'
url="http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=$keyword&retmax=2147483647&retmode=json"

wget -qO- "$url" | ./esrchjson2ids.py | while IFS=$'\t' read -r id
do
	wget -qO- "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gds&id=$id&retmode=json" | ./gdsesmryjson2tsv0.py
done | sort -k 1,1 

#> result.txt 
